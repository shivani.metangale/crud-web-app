import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http : HttpClient) { }
  // to post the data on server(json)
  postUser(data : any){
    return this.http.post<any>("http://localhost:3000/userList/",data);
  }
  // to get the form records from
  getUser(){
    return this.http.get<any>("http://localhost:3000/userList/");   
  }
  // to update the data while editing
  putUser(data:any,id : number){
    return this.http.put<any>("http://localhost:3000/userList/"+id, data);
  }
  deleteUser(id:number){
    return this.http.delete<any>("http://localhost:3000/userList/"+id);
  }
  postTask(data : any){
    console.log("hi",data)
    return this.http.post<any>("http://localhost:3000/taskList/",data);
  }
  getTask(id: number) {
    return this.http.get("http://localhost:3000/taskList?userId=" +id);
  }
  putTask(data:any,id : number){
    return this.http.put<any>("http://localhost:3000/taskList/"+id, data);
  }
  // :observable Observable<any>
  deleteTask(id:number){ 
    return this.http.delete<any>("http://localhost:3000/taskList/"+id); 
  }

}
