
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { ApiService } from '../services/api.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { ConfirmdeleteComponent } from '../confirmdelete/confirmdelete.component';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-usertable',
  templateUrl: './usertable.component.html',
  styleUrls: ['./usertable.component.css']
})
export class UsertableComponent implements OnInit {
  title = 'webapp';
  displayedColumns: string[] = ['FirstName', 'LastName', 'Email', 'action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dialog: MatDialog, private api: ApiService, private _snackBar: MatSnackBar) {

  }
  ngOnInit(): void {
    this.getAllUsers();
  }
  openDialog() {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '30%'


    }).afterClosed().subscribe(val => {
      if (val === 'save') {
        this.getAllUsers();
        this.openSnackBar("Added successfully", "Dismiss")
      }
    })
    // dialogRef.afterClosed().subscribe(result => {
    //   console.log("Dialog Closed");
    // });
  }
  // to get all the data on UI
  getAllUsers() {
    this.api.getUser()
      .subscribe({
        next: (res) => {
          this.dataSource = new MatTableDataSource(res);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        },
        error: (err) => {
          alert("Error while fetching the Records!")

        }
      })

  }
  editUser(row: any) { 
    this.dialog.open(DialogComponent, {
      width: '30%',
      // passing the row value
      data: row
    }).afterClosed().subscribe(val => {
      if (val === 'update') {
        this.getAllUsers();
        this.openSnackBar("Updated Succesfully","Dismiss")
       }
    })
  }
  deleteUser(id: number) {

    this.api.getTask(id).subscribe((
        res: any
      ) => {
        if (res[0]) {
          // alert("User cannot be deleted");
          this.openSnackBar("User Cannot be deleted","Dismiss")
        } else {
          this.dialog.open(ConfirmdeleteComponent, {
              width: "30%",
              data: { id: id,user:true}
            }).afterClosed().subscribe(val => {
              if (val) {
                this.api.deleteUser(id).subscribe({
                  next: (res) => {
                    this.openSnackBar("Deleted successfully","Dismiss")
                    // alert("Deleted successfully");
                    this.getAllUsers();
                  },
                  error: () => {
                    alert("Error while deleting the record!")
                  }
                })

              }

            })

        }
      }
    )
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
}

