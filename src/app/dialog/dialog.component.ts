import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  userForm !: FormGroup;
  actionBtn : string = "Save"
  MatdialogRef: any;
  dialogRef: any;
  constructor(private matDialog : MatDialogRef<DialogComponent>,
     private formBuilder : FormBuilder,
      private api : ApiService,
      @Inject(MAT_DIALOG_DATA) public editData : any) { }

  ngOnInit(): void {
    this.userForm = this.formBuilder.group({
      FirstName : ['',Validators.required],
      LastName : ['',Validators.required],
      Email : ['',Validators.required]
      



    });
    if(this.editData){
      this.actionBtn = "Update";
      this.userForm.controls['FirstName'].setValue(this.editData.FirstName);
      this.userForm.controls['LastName'].setValue(this.editData.LastName);
      this.userForm.controls['Email'].setValue(this.editData.Email);
      
      // to patch all the row value while updating

    }
  }
  addUser(){
    if(!this.editData){
      if(this.userForm.valid){
        this.api.postUser(this.userForm.value)
        .subscribe({
          next:(res)=>{
            // alert("User added successfully");
            this.userForm.reset();
            this.matDialog.close('save');
          },
          error:()=>{
            alert("Error while adding the User")
          }
        })
      }
    }else{
      this.updateUser()
    }
    
  }
  updateUser(){
    this.api.putUser(this.userForm.value,this.editData.id)
    .subscribe({
      next:(res)=>{
        // alert("Data Updated Successfully");
        this.userForm.reset();
        this.matDialog.close('update');
       
      },
      error:()=>{
        alert("Error while updating the record!");

      }
    })

  }

}
