import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AppComponent } from './app.component';
import { TaskComponent } from './task/task.component';
import { UsertableComponent } from './usertable/usertable.component';

const routes: Routes = [{
  path: "",
  component: UsertableComponent,
},
{
  path: ":userId",
  component: TaskComponent,
},];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
