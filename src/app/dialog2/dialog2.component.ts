
import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators } from '@angular/forms';
import { ApiService } from '../services/api.service';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-dialog2',
  templateUrl: './dialog2.component.html',
  styleUrls: ['./dialog2.component.css']
})
export class Dialog2Component implements OnInit {

  

    taskForm !: FormGroup;
    actionBtn : string = "Save"
    MatdialogRef: any;
    dialogRef: any;
    constructor(private matDialog : MatDialogRef<Dialog2Component>,
       private formBuilder : FormBuilder,
        private api : ApiService,
        @Inject(MAT_DIALOG_DATA) public editData : any) { }
  
    ngOnInit(): void {
      this.taskForm = this.formBuilder.group({
        Task : ['',Validators.required],
        Description : ['',Validators.required],
        
        
  
  
  
      });
      if(this.editData.id){
        this.actionBtn = "Update";
        this.taskForm.controls['Task'].setValue(this.editData.Task);
        this.taskForm.controls['Description'].setValue(this.editData.Description);
        
  
      }
    }
    addTask(){
      this.taskForm.value.userId= this.editData.userId
      if(!this.editData.id){
        if(this.taskForm.valid){
          this.api.postTask(this.taskForm.value)
          .subscribe({
            next:(res)=>{
              // alert("Task added successfully");
              this.taskForm.reset();
              this.matDialog.close('save');
            },
            error:()=>{
              alert("Error while adding the User")
            }
          })
        }
      }else{
        this.updateTask()
      }
      
    }
    updateTask(){
      
      this.api.putTask(this.taskForm.value,this.editData.id)
      .subscribe({
        next:(res)=>{
          // alert("Data Updated Successfully");
          this.taskForm.reset();
          this.matDialog.close('update');
         
        },
        error:()=>{
          alert("Error while updating the record!");
  
        }
      })
  
    }
  
  }
  
  


