import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { ApiService } from '../services/api.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { ActivatedRoute } from "@angular/router";
import { Dialog2Component } from '../dialog2/dialog2.component';
import { ConfirmdeleteComponent } from '../confirmdelete/confirmdelete.component';
import {MatSnackBar} from '@angular/material/snack-bar';


@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  title = 'webapp';
  userId!: any;
  displayedColumns: string[] = ['Task', 'Description', 'action'];
  dataSource!: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(private dialog : MatDialog, private api : ApiService, private activatedRoute: ActivatedRoute,private _snackBar: MatSnackBar){

  }
  ngOnInit(): void {
    this.userId = this.activatedRoute.snapshot.params["userId"];
    this.getAllTask();
  }
  openDialog() {
    let dialogRef=this.dialog.open(Dialog2Component, {
      width:'30%',
      data: {
        userId: this.userId
      }
    
      
    }).afterClosed().subscribe(val=>{
      if(val ==='save'){
        this.getAllTask();
        this.openSnackBar("Added Successfully","Dismiss")
      }
    })
    
  }
  getAllTask(){
    // console.log(this.userId)
    this.api.getTask(this.userId)
    .subscribe({
      next:(res: any)=>{
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      error:(err)=>{
        alert("Error while fetching the Records!")

      }
    })

  }
  editTask(row : any){
    this.dialog.open(Dialog2Component,{
      width: '30%',
      data:row
    }).afterClosed().subscribe(val=>{
      if(val==='update'){
        this.getAllTask();
        this.openSnackBar("Updated Successfully","Dismiss")
        
      }
    })
  }
  deleteTask(id:number){
    
    this.dialog
      .open(ConfirmdeleteComponent, {
        width: "30%",
        data: { id: id },
      })
      .afterClosed()
    .subscribe({
      next:(res)=>{
        if(res==="delete"){
          this.api.deleteTask(id).subscribe(() => {
             console.log("Task Deleted")
             this.openSnackBar("Deleted Successfully","Dismiss")
        // alert("Deleted successfully");
             this.getAllTask();
        
      })
          

        }
      },
      error:()=>{
        alert("Error while deleting the record!")
      }
    })

  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action);
  }
    


  
  

}
  

